// Task 1.
type ConcatFn = (firstString: string, secondString: string) => string

const concat: ConcatFn = (firstString, secondString) => firstString + secondString

const result = concat('Hello', 'World')

// Task 2.

interface IWithData {
    howIDoIt: string,
    simeArray: Array<string | number>
}

interface IMyHometask {
    howIDoIt: string,
    simeArray: Array<string | number>,
    withData: Array<IWithData>
}

const MyHometask: IMyHometask = {
    howIDoIt: "I Do It Wel",
    simeArray: ["string one", "string two", 42],
    withData: [{ howIDoIt: "I Do It Wel", simeArray: ["string one", 23] }],
}

// Task 3.
interface MyArray<T> {
    [N: number]: T

    reduce<U>(fn: (accumulator: U, value: U) => U, initialValue?: U): U
}

const myArray: MyArray<number> = [1,2,3]

const sum = myArray.reduce<string>((acc, val) => acc + val)

// Task 4.

interface IHomeTask {
    data: string;
    numbericData: number;
    date: Date;
    externalData: {
        basis: number;
        value: string;
    }
}

type MyPartial<T> = {
    [N in keyof T]?: MyPartial<T[N]>
}


const homeTask: MyPartial<IHomeTask> = {
    externalData: {
        value: 'win'
    }
}

// TODO: Task 5,6
