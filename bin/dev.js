const express = require('express')
const webpack = require('webpack')
const [webpackClientConfig, webpackServerConfig] = require('../webpack.config')
const path = require('path')
const nodemon = require('nodemon')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')

const hmrServer = express()
const clientCompiler = webpack(webpackClientConfig)


hmrServer.use(webpackDevMiddleware(clientCompiler, {
    publicPath: webpackClientConfig.output.publicPath,
    serverSideRender: true,
    writeToDisk: true,
    noInfo: true,
    watchOptions: {
      ignore: /dist/
    },
    stats: 'errors-only'
}))

hmrServer.use(webpackHotMiddleware(clientCompiler, {
    path: '/static/__webpack_hmr'
}))

hmrServer.listen(3001, () => {
    console.log('HMR server successfully started')
})

const compiler = webpack(webpackServerConfig)

compiler.run((error) => {
    if (error) {
        console.log('Compilation failed: ', error)
    }

    compiler.watch({}, (error) => {
        if (error) {
            console.log('Compilation failed: ', error)
        }
        console.log('Compilation was successfully')
    })

    nodemon({
        script: path.resolve(__dirname, '../dist/server/server.js'),
        watch: [
            path.resolve(__dirname, '../dist/server'),
            path.resolve(__dirname, '../dist/client')
        ]
    })
})


